<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap PHP $_SERVER </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="jumbotron text-center">
  <h1>Super Global $_SERVER Display</h1>
  <p>Litong Zhang </p> 
</div>
  
<div class="container">
  <div class="row">
    
    <div class="col-sm-3">
      <h3>PHP_SELF</h3>
      <p><?php echo $_SERVER['PHP_SELF']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>GATEWAY_INTERFACE</h3>
      <p><?php echo $_SERVER['GATEWAY_INTERFACE']; ?></p>     
    </div>
    <div class="col-sm-3">
      <h3>SERVER_ADDR</h3> 
      <p><?php echo $_SERVER['SERVER_ADDR']; ?></p>       
    </div><div class="col-sm-3">
      <h3>SERVER_SOFTWARE</h3>        
      <p><?php echo $_SERVER['SERVER_SOFTWARE']; ?></p>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-3">
      <h3>SERVER_NAME</h3>
      <p><?php echo $_SERVER['SERVER_NAME']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>SERVER_PROTOCOL</h3>
      <p><?php echo $_SERVER['SERVER_PROTOCOL']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>REQUEST_METHOD</h3>        
      <p><?php echo $_SERVER['REQUEST_METHOD']; ?></p>
    </div><div class="col-sm-3">
      <h3>REQUEST_TIME</h3>        
      <p><?php echo $_SERVER['REQUEST_TIME']; ?></p>
    </div>
  </div>

<div class="row">
    <div class="col-sm-3">
      <h3>DOCUMENT_ROOT</h3>
      <p> <?php echo $_SERVER['DOCUMENT_ROOT']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>ACCEPT_ENCODING</h3>
      <p><?php echo $_SERVER['HTTP_ACCEPT_ENCODING']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>HTTP_HOST</h3>        
      <p><?php echo $_SERVER['HTTP_HOST']; ?></p>
    </div><div class="col-sm-3">
      <h3>HTTP_CONNECTION</h3>        
      <p><?php echo $_SERVER['HTTP_CONNECTION']; ?></p>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-3">
      <h3>ACCEPT_LANGUAGE</h3>
      <p><?php echo $_SERVER['HTTP_ACCEPT_LANGUAGE']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>REMOTE_ADDR</h3>
      <p><?php echo $_SERVER['REMOTE_ADDR']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>REMOTE_PORT</h3>        
      <p><?php echo $_SERVER['REMOTE_PORT']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>SERVER_SIGNATURE</h3>        
      <p><?php echo $_SERVER['SERVER_SIGNATURE']; ?></p>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-3">
      <h3>SCRIPT_FILENAME</h3>
      <p><?php echo $_SERVER['SCRIPT_FILENAME']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>SERVER_ADMIN</h3>
      <p><?php echo $_SERVER['SERVER_ADMIN']; ?></p>
    </div>
    <div class="col-sm-3">
      <h3>SERVER_PORT</h3>        
      <p> <?php echo $_SERVER['SERVER_PORT']; ?></p>
    </div><div class="col-sm-3">
      <h3>REQUEST_URI</h3>        
      <p><?php echo $_SERVER['REQUEST_URI']; ?></p>
    </div>
  </div>

</div>

</body>
</html>
